resource "google_compute_disk" "disk" {
  name  = "disk"
}

resource "google_compute_attached_disk" "default" {
  disk     = google_compute_disk.disk.id
  instance = module.web.web_id
}