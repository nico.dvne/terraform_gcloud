resource "google_compute_firewall" "allow-http" {
  name    = "firewall-http"
  network = module.vpc.vpc_id

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["80", "8080"]
  }

  source_ranges = ["0.0.0.0/0"]
  target_tags = ["web"]
}

resource "google_compute_firewall" "allow-ssh" {
  name    = "firewall-ssh"
  network = module.vpc.vpc_id

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["22"]
  }

  source_ranges = ["0.0.0.0/0"]
  target_tags = ["bastion"]
}

resource "google_compute_firewall" "ssh-bastion-web" {
  name        = "firewall-bastion-web"
  network     = module.vpc.vpc_id

  allow {
    protocol  = "tcp"
    ports     = ["22"]
  }

  source_tags = ["foo"]
  target_tags = ["web"]
}