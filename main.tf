module "vpc" {
  source = "./modules/vpc"
}

module "bastion" {
  source = "./modules/instances"
  name = "bastion"
  tags = ["bastion"]
  network = module.vpc.vpc_id
}

module "web" {
  source = "./modules/instances"
  name = "web"
  tags = ["web"]
  network = module.vpc.vpc_id
  instance_count = 3
}
