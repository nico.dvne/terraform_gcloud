resource "google_compute_instance" "vm-instance" {

  count = var.instance_count
  name         = "${var.name}-${count.index}"
  machine_type = var.type_compute
  zone         = "us-central1-c"


  tags         = var.tags

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-9"
    }
  }

  metadata = {
      ssh-keys = "insset:${file("config/public_key.pub")}"
  }

  network_interface {
    network = var.network

    access_config {
      // Ephemeral public IP
    }
  }
}