output "tags" {
    value = google_compute_instance.vm-instance[0].name
}

output "web_id" {
  value = google_compute_instance.vm-instance[0].id
}