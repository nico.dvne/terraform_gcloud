variable "name" {
    description = "Nom de l'instance"
    type        = string
}

variable "type_compute" {
    description = "Type de la VM"
    type        = string
    default     = "e2-micro"
}

variable "tags" {
    description = "Tags de l'instance"
    type        = list
}

variable "network" {
    type = string
}

variable "instance_count" {
    type = number
    default = 1
}
