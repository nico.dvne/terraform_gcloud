resource "google_compute_network" "instance_vpc" {
  name = "vpc-network"
}

resource "google_compute_subnetwork" "subnet" {
  name          = "subnetwork"
  ip_cidr_range = "10.2.0.0/16"
  region        = "us-central1"
  network       = google_compute_network.instance_vpc.id
}