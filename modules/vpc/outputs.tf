output "vcp_name" {
    value = google_compute_network.instance_vpc.name
}

output "vpc_id" {
  value = google_compute_network.instance_vpc.id
}

output "sub_name" {
    value = google_compute_subnetwork.subnet.name
}

output "sub_id" {
  value = google_compute_subnetwork.subnet.id
}